class Backend {
  /**
   * @constructor
   * @param {Sms} sms - An instance of Sms to use.
   */
  constructor(sms) {
    this.sms = sms
  }

  /**
   * @returns {Promise<Void>}
   */
  refresh() {
    return new Promise((resolve, reject) => {
    })
  }

  /**
   * @returns {Promise<List<Thread>>}
   */
  threads() {
    return new Promise((resolve, reject) => {
    })
  }

  /**
   * @param {Number} threadId - A unique ID of a thread from which to fetch
   *                            message history.
   * @returns {Promise<List<Message>>}
   */
  messages(threadId) {
    return new Promise((resolve, reject) => {
    })
  }
}

module.exports = Backend
