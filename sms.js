const spawn = require('child_process').spawn

/*
 * Message
  {
    threadid:   Number,
    type:       inbox|sent|draft|outbox,
    read:       Boolean,
    number:     String
    received:   String
    body:       String,
    _id:        Number
  }

 * Contact
  {
    name:       String,
    number:     String
  }
*/

class Sms {
  /**
   * @constructor
   * @param {String} sshTarget - The host to SSH into that's running termux.
   */
  constructor(sshTarget) {
    this.sshTarget = sshTarget
  }

  /**
   * @returns {Promise<List<Contact>>}
   */
  contacts() {
    return new Promise((resolve, reject) => {
      const p = spawn('ssh',
        [this.sshTarget, 'termux-contact-list'])
      let text = ''
      p.stdout.on('data', buf => {
        text += buf.toString()
      })
      p.once('exit', code => {
        if (code === 0) {
          try {
            const msgs = JSON.parse(text)
            resolve(msgs)
          } catch (err) {
            reject(err)
          }
        } else reject(new Error('exit code ' + code))
      })
    })
  }

  /**
   * @param {Number} offset - How many messages from latest to skip.
   * @param {Number} limit - How many messages to return at the most.
   * @returns {Promise<List<Message>>}
   */
  read(offset, limit) {
    return new Promise((resolve, reject) => {
      const p = spawn('ssh',
        [this.sshTarget, 'termux-sms-list', '-o', offset, '-l', limit])
      let text = ''
      p.stdout.on('data', buf => {
        text += buf.toString()
      })
      p.once('exit', code => {
        if (code === 0) {
          try {
            const msgs = JSON.parse(text)
            resolve(msgs)
          } catch (err) {
            reject(err)
          }
        } else reject(new Error('exit code ' + code))
      })
    })
  }

  /**
   * @param {String} Number - The phone # to send text to.
   * @param {String} text - The message to send to number.
   * @returns {Promise<Void>} - Whether sending succeeded.
   */
  write(number, text) {
    return new Promise((resolve, reject) => {
      text = text.replace(/'/g, '\\\'')
      const p = spawn('ssh',
        [this.sshTarget, 'termux-sms-send', '-n', number, text])
      p.once('exit', code => {
        if (code === 0) resolve()
        else reject(new Error('exit code ' + code))
      })
    })
  }
}

module.exports = Sms
