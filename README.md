# sms-interface
> Spiffier name pending!

At the time of writing, I've had fairly chronic wrist & hand pain, and picking
up my android phone, unlocking it, opening up the SMS app, and typing out
messages on a little screen hasn't been good for my body.

It would be great if I could use a local program on my laptop to interace with
SMS instead, so I can use a more comfortable keyboard and user interface.

There are some apps on the Play Store that do this, but everything I've found
has either been proprietary & closed source, or quite large and (imo)
over-engineered. The Play Store also requires a Google account to use.

This program aims to provide as simple and lightweight of an interface as
possible, with minimal dependencies, that lends itself to modification.

((FORTHCOMING: screenshot))

## Dependencies
### Android Device
- [Termux](https://f-droid.org/en/packages/com.termux/)
- [Termux:API](https://f-droid.org/en/packages/com.termux.api/)
- An SSH server installed & running
  - Once Termux is installed, you can do this with `pkg install sshd && sshd`
### Desktop
- NodeJS
- SSH

